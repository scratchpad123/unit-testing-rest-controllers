package com.example.presentation.web.representations;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SimpleProductRepresentation {
    private Long id;
    private String name;
    private String picture;
    private BigDecimal price;
}
