package com.example.presentation.web.mappers;

import com.example.presentation.domain.Product;
import com.example.presentation.web.representations.DetailedProductRepresentation;
import com.example.presentation.web.representations.SimpleProductRepresentation;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {
    public static SimpleProductRepresentation productToSimpleRepresentation(Product product) {
        final var productRepresentation = new SimpleProductRepresentation();

        productRepresentation.setId(product.getId());
        productRepresentation.setName(product.getName());
        productRepresentation.setPicture(product.getPicture());
        productRepresentation.setPrice(product.getPrice());

        return productRepresentation;
    }

    public static DetailedProductRepresentation productToDetailRepresentation(Product product) {
        final var productRepresentation = new DetailedProductRepresentation();

        productRepresentation.setId(product.getId());
        productRepresentation.setName(product.getName());
        productRepresentation.setPicture(product.getPicture());
        productRepresentation.setPrice(product.getPrice());
        productRepresentation.setBarcode(product.getBarcode());

        return productRepresentation;
    }

    public SimpleProductRepresentation toSimpleRepresentation(Product product) {
        return ProductMapper.productToSimpleRepresentation(product);
    }

    public DetailedProductRepresentation toDetailRepresentation(Product product) {
        return ProductMapper.productToDetailRepresentation(product);
    }
}
