package com.example.presentation.web;

import com.example.presentation.service.ProductService;
import com.example.presentation.web.mappers.ProductMapper;
import com.example.presentation.web.representations.DetailedProductRepresentation;
import com.example.presentation.web.representations.SimpleProductRepresentation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    private final ProductMapper productMapper;

    @GetMapping("/products")
    public Stream<SimpleProductRepresentation> getProductList() {

        return productService.getProductList().stream()
                .map(productMapper::toSimpleRepresentation);
    }

    @GetMapping("/products/{id}")
    public DetailedProductRepresentation getProductDetails(@PathVariable long id) {

        var product = productService.getProduct(id);

        return productMapper.toDetailRepresentation(product);
    }

    @GetMapping("/product-of-the-week")
    public DetailedProductRepresentation getProductOfTheWeek() {
        
        var product = productService.getProductOfTheWeek();

        return productMapper.toDetailRepresentation(product);
    }
}
