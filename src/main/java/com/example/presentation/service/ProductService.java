package com.example.presentation.service;

import com.example.presentation.domain.Product;
import com.example.presentation.domain.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository repository;

    public List<Product> getProductList() {
        return repository.getProductList();
    }

    public Product getProduct(Long id) {
        return repository.getProduct(id);
    }

    public Product getProductOfTheWeek() {
        return repository.getProductOfTheWeek();
    }
}
