package com.example.presentation.domain;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepository {
    public List<Product> getProductList() {
        return new ArrayList<>();
    }

    public Product getProduct(Long id) {
        return new Product();
    }

    public Product getProductOfTheWeek() {
        return new Product();
    }
}
