package com.example.presentation.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {
    private Long id;
    private String name;
    private String picture;
    private BigDecimal price;
    private String barcode;
}
