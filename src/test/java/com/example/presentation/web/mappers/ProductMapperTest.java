package com.example.presentation.web.mappers;

import com.example.presentation.domain.Product;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith({MockitoExtension.class})
class ProductMapperTest {

    private static final Long PRODUCT_ID = 1L;
    private static final String PRODUCT_NAME = "Product";
    private static final String PRODUCT_PICTURE = "image01.png";
    private static final BigDecimal PRODUCT_PRICE = BigDecimal.TEN;
    private static final String PRODUCT_BARCODE = "0123456789012";

    @InjectMocks
    private ProductMapper productMapper;

    @Test
    void productToSimpleRepresentation() {
        final Product product = createProduct();

        val representation = productMapper.toSimpleRepresentation(product);

        assertThat(representation.getId()).isEqualTo(PRODUCT_ID);
        assertThat(representation.getName()).isEqualTo(PRODUCT_NAME);
        assertThat(representation.getPicture()).isEqualTo(PRODUCT_PICTURE);
        assertThat(representation.getPrice()).isEqualTo(PRODUCT_PRICE);
    }

    @Test
    void productToDetailRepresentation() {
        final Product product = createProduct();

        val representation = productMapper.toDetailRepresentation(product);

        assertThat(representation.getId()).isEqualTo(PRODUCT_ID);
        assertThat(representation.getName()).isEqualTo(PRODUCT_NAME);
        assertThat(representation.getPicture()).isEqualTo(PRODUCT_PICTURE);
        assertThat(representation.getPrice()).isEqualTo(PRODUCT_PRICE);
        assertThat(representation.getBarcode()).isEqualTo(PRODUCT_BARCODE);
    }

    private Product createProduct() {
        final Product product = new Product();

        product.setId(PRODUCT_ID);
        product.setName(PRODUCT_NAME);
        product.setPicture(PRODUCT_PICTURE);
        product.setPrice(PRODUCT_PRICE);
        product.setBarcode(PRODUCT_BARCODE);

        return product;
    }
}