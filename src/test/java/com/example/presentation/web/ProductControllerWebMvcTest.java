package com.example.presentation.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
class ProductControllerWebMvcTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductController controller;

    @Test
    void getProductList() throws Exception {
        mockMvc.perform(get("/products"))
                .andExpect(status().is2xxSuccessful());

        verify(controller).getProductList();
    }

    @Test
    void getProductDetails() throws Exception {
        mockMvc.perform(get("/products/5"))
                .andExpect(status().is2xxSuccessful());

        verify(controller).getProductDetails(5);
    }

    @Test
    void getProductOfTheWeek() throws Exception {
        mockMvc.perform(get("/product-of-the-week"))
                .andExpect(status().is2xxSuccessful());

        verify(controller).getProductOfTheWeek();
    }
}