package com.example.presentation.web;

import com.example.presentation.domain.Product;
import com.example.presentation.service.ProductService;
import com.example.presentation.web.mappers.ProductMapper;
import com.example.presentation.web.representations.DetailedProductRepresentation;
import com.example.presentation.web.representations.SimpleProductRepresentation;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductControllerTest {

    @Mock
    private ProductService productService;

    @Mock
    private ProductMapper productMapper;

    @InjectMocks
    private ProductController productController;

    private Product mockProduct;
    private SimpleProductRepresentation expectedSimpleProductRepresentation;
    private DetailedProductRepresentation expectedDetailedProductRepresentation;

    @BeforeEach
    void setUp() {
        mockProduct = mock(Product.class);
        expectedSimpleProductRepresentation = new SimpleProductRepresentation();
        expectedDetailedProductRepresentation = new DetailedProductRepresentation();

        lenient().when(productMapper.toSimpleRepresentation(mockProduct)).thenReturn(expectedSimpleProductRepresentation);
        lenient().when(productMapper.toDetailRepresentation(mockProduct)).thenReturn(expectedDetailedProductRepresentation);

    }

    @Test
    void getProductList() {
        when(productService.getProductList()).thenReturn(List.of(mockProduct));

        val result = productController.getProductList();

        assertThat(result).containsExactly(expectedSimpleProductRepresentation);
    }

    @Test
    void getProductDetails() {
        when(productService.getProduct(5L)).thenReturn(mockProduct);

        val result = productController.getProductDetails(5L);

        assertThat(result).isEqualTo(expectedDetailedProductRepresentation);
    }

    @Test
    void getProductOfTheWeek() {
        when(productService.getProductOfTheWeek()).thenReturn(mockProduct);

        val result = productController.getProductOfTheWeek();

        assertThat(result).isEqualTo(expectedDetailedProductRepresentation);
    }
}