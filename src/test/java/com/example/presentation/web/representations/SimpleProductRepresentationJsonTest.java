package com.example.presentation.web.representations;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@JsonTest
class SimpleProductRepresentationJsonTest {

    @Autowired
    private JacksonTester<SimpleProductRepresentation> jsonTester;

    @Test
    void testSerialization() throws Exception {
        final SimpleProductRepresentation representation = new SimpleProductRepresentation();

        representation.setId(1L);
        representation.setName("Product");
        representation.setPicture("image01.png");
        representation.setPrice(BigDecimal.TEN);

        val json = jsonTester.write(representation);

        assertThat(json)
                .hasJsonPathNumberValue("$.id", 1)
                .hasJsonPathStringValue("$.name", "Product")
                .hasJsonPathStringValue("$.picture", "image01.png")
                .hasJsonPathNumberValue("$.price", 10);
    }
}