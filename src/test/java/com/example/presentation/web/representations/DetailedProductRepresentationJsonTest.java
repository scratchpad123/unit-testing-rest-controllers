package com.example.presentation.web.representations;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@JsonTest
class DetailedProductRepresentationJsonTest {

    @Autowired
    private JacksonTester<DetailedProductRepresentation> jsonTester;

    @Test
    void testSerialization() throws Exception {
        final DetailedProductRepresentation representation = new DetailedProductRepresentation();

        representation.setId(1L);
        representation.setName("Product");
        representation.setPicture("image01.png");
        representation.setPrice(BigDecimal.TEN);
        representation.setBarcode("0123456789012");

        val json = jsonTester.write(representation);

        assertThat(json)
                .hasJsonPath("$.id", 1)
                .hasJsonPath("$.name", "Product")
                .hasJsonPath("$.picture", "image01.png")
                .hasJsonPath("$.price", 10)
                .hasJsonPath("$.barcode", "0123456789012");
    }
}